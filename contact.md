---
layout: page
title: Contact
permalink: /contact/
img: npapoylias.jpg
---


**Multi-Ordered Grammars, the Gray algorithm, as well as GrammarTraits, MOODs and Lise** are the brain-children of Nick Papoulias.

**Nick is a computer scientist with an expertise on Programming Languages & Tools.** He has been experimenting with different aspects of the programming cycle, with a primary focus on **parsing, reflection, debugging and simulation**. He is currently investigating the design and use of domain-specific languages for interdisciplinary research.

**[For more info visit Nick's personal web-site](https://parsenet.info){: target="_blank"} or contact him through:**

E-mail: **npapoylias[at]gmail[dot]com**

Twitter: **@npapoylias**
