---
layout: page
title: Crowd-Funding Campaign
permalink: /Support/
---

I have set-up a crowd-funding campaign to support a **stable release** for Lan.d.s and the 
authoring of the book: **["Theory & Applications of Multi-Ordered Grammars"]({{site.baseurl}}/Book/)**. 

You can pledge your support either through **[Patreon](https://www.patreon.com/bePatron?u=14738200&redirect_uri=http%3A%2F%2F127.0.0.1%3A4000%2Flands-project%2FSupport%2F&utm_medium=widget) 
or PayPal** below: 

<table class="center" style="margin-top:-10px">
	<tr><!--<td style="background-color:#f96854;">
	<a href="https://www.patreon.com/bePatron?u=14738200" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script></td>-->
<td><!--style="background-color:#f96854;"-->
	<a href="https://www.patreon.com/bePatron?u=14738200" data-patreon-widget-type="become-patron-button"><img width="420px" height="52px" src="{{site.baseurl}}/images/support_patreon.png"></a>
<!--<script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>--></td>
</tr>
<tr><td>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="CTYLDP9XUKCBS">
<table>
<tr><td><input type="hidden" name="on0" value="Crowd-Fund Book, for 6 months to get:"><b>or subscribe for 6 months on PayPal for:</b></td></tr><tr><td>
<div class="custom-select" style="width:420px;">
<select name="os0">
	<option value="Ebook">Ebook : €5,00 EUR - monthly</option>
	<option value="Printed Book">Printed Book : €10,00 EUR - monthly</option>
	<option value="Institutional">Institutional : €100,00 EUR - monthly</option>
	<option value="Consultancy">Consultancy : €1 000,00 EUR - monthly</option> 
	<option value="Sponsorship">Sponsorship : €2 000,00 EUR - monthly</option>
</select></div></td></tr>
</table>
<input type="hidden" name="currency_code" value="EUR">
<input type="image" src="{{site.baseurl}}/images/support_paypal.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="margin-top: 10px" width="420px" height="80px">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
</td>
</tr>
</table>

<p class="center">Alternatively you can make a direct contribution through paypal.me: <b><a href="https://www.paypal.me/npapoylias">PayPal.Me for Lands.</a></b> Adding <b>"Lands Support"</b> as a note, plus the <b>e-mail and address</b> you would wish to receive your perks on.</p>

All **additional chapters or lecture materials that I will record will be available for FREE to all subscribers**. Here is a break-down of the different Tiers for individuals, institutions and coorporations:

<a class="lightbox" href="{{site.baseurl}}/images/book-patreon.png">
   <img src="{{site.baseurl}}/images/book-patreon.png"/>
</a>


We are going to cover [MOGs, Gray, ASL]({{site.baseurl}}/Gray) and [GrammarTraits]({{site.baseurl}}/GrammarTraits) in great detail inside the book. We will also **build together line by line at least 6 languages**, ranging from prototype-based and class-based OO languages and extensions, to [Domain-specific]({{site.baseurl}}/MOODs), functional and [multi-paradigm systems like Lise]({{site.baseurl}}/Lise).

**Tiers begin at 5 euros per chapter for e-books** in all formats including the interactive TOC-Boards that are developed within the platform. Total pledges in this tier will not surpass 30 euros in total since everything after the first 6 chapters will be free for all supporters. 

**The printed/ebook combo comes in 10 euros per chapter**, which I will distribute through [blurb.com](https://www.blurb.com). Again here total pledges cannot surpass 60 euros, since everything after the first 6 chapters will be free. 

**For all interested academic, research or other institutions the eBook will be distributed for free**, as well as the inclusion to the Lands research network, with the possibility of an on-site lecture (if you can cover travel expenses). **But !! If you do have funding to support the project, consider joining at 100 euros / chapter** (which will not surpass 600 euros of total support). 

Finally, if you and your company are making a living through parsing technologies (or you imagine doing so in the future) **Lan.d.s will be an excellent R&D investement for you !** Your pledge can give you **6 to 12 days of consultancy (in total) for any parsing project you might have**, an on-site 1-day workshop, plus networking and sponsoring logos on the Lands portal. 

For any additional crazy specialized support you might need (like porting / embedding re-licensing etc) contact me directly at: [npapoylias@gmail.com](npapoylias@gmail.com) and we may find a way to make it happen.
