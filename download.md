---
layout: page
title: Download
permalink: /download/
---


<div class="jumbotron">
  <h1>Lands-Alpha is now available !</h1>
  <p>All experiments and tests documented in this website are readily reproducible,
on your favorite platform (Linux, Mac, Windows)</p>
  <p><a class="btn btn-primary btn-lg" href="{{site.baseurl}}/Lands-Alpha-All-Platforms.zip">Download now</a></p>
</div>

After downloading, simply double-click (or run in the cmd-line) 
the corresponding launcher for your OS:

**Linux** (tested on Ubuntu 18.04): **Lands-Alpha-Linux**

**Mac** (tested on High Sierra): **Lands-Alpha-Mac**

**Win** (tested on Win10): **Lands-Alpha-Win.lnk**

<br>

