---
layout: post
title: Lise
img: lise.png
---

**Lise** (short for (list (embedding)) is **a dynamic functional-first, byte-compiled language, embedded in an
extented version of the pure OO environment of Pharo**. Lise pushes MOGs and
ASL to their limits by **exploring the ambiguity, conflicts and
interplay of multi-paradigm (functional, OO, procedural) language design**.

<blockquote>
<b>Lise raison d'être</b> ? Study <a href="https://mitpress.mit.edu/sites/default/files/sicp/index.html"> SICP </a> with a <a href="https://www.youtube.com/watch?v=JLPiMl8XUKU">Xerox flavor</a>, while re-inventing some <a href="https://www.youtube.com/watch?v=JoVQTPbD6UY">Bell labs</a> utilities. 
</blockquote>

Here is the **extented version of the postcard** we saw earlier, this time **written entirely in Lise**:

![lisePostcard]({{site.baseurl}}/images/lisePostcard.png)

For **quick comparison**, here is the **pure Pharo version**:

![gTraitsPostcard]({{site.baseurl}}/images/gTraitsPostcard.png)

**Right from this first example**, we can see some **intresting new semantics
that are made possible from the compiled embedding** of a lisp dialect into
an OO envrionment, **that are not found in a OO-only or functional-only environments**:

![lisePostcardAnnotated]({{site.baseurl}}/images/lisePostcardAnnotated.png)

<div class="panel panel-primary">
    <div class="panel-heading">
    <h3 class="panel-title">Smiley Scoping</h3>
    </div>
    <div class="panel-body">
<p>The most obvious is <b><u>the smiley (: alternative to parentesized function calls</u></b>, 
that serves <b>both for scoping Lise</b> syntax within Pharo code:</p>

<p>In the example above the return caret is a Pharo statement, while
the rest of the method from <b><mark style="color:blue">(:begin ...` till the end )</mark></b> is 
a Lise expression.</p>

<p><b>But also for scoping Pharo</b> syntax from within Lise:</p>

<p>In the example above this can be seen in the <b><mark style="color:blue">(:t copyFrom: 1 to: t size -1)
</mark></b> Pharo expression, embedded within the wider Lise expression.</p>

<p>As we will see later <b>this scoping can be extented 
to accomodated other kind of syntaxes as well.</b> It also happens to be a <b>perfect starting point for researching ambiguity arising in multi-paradigm language design</b>, since (at least in this case) <b>the explicity scoping can be infered at run-time</b> even if parsing is ambiguous at compile-time.</p>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
    <h3 class="panel-title">Polymorphic Functional Dispatch</h3>
    </div>
    <div class="panel-body">

<p>Since Lise is embedded inside an OO language, <b><u>there is no reason to restrict
ourselves only to lambdas as heads of functional invocations.</u></b></p>

<p>In fact in Lise, <b>every entity responding to the Lise-specific <u>#polyValueWithArguments:</u> message</b>
can be used as a functional application head. This includes: <ol> <li><b>Lambdas</b> (this is exactly how normal functional
invocation works in Lise)</li> <li><b>Symbols</b> (which accepts an object to be applied
to reflectively as a second argument, plus the rest of the arguments if any).</li> <li><b>Every other Pharo object</b> 
(a default #polyValueWithArguments: message is defined in the root hierarch accepting a symbol to 
reflectively perform as a second argument, plus the rest of the arguments if applicable)</li></ol></p>

<p>These can be seen in numerous places in the above example, such as <b><mark style="color:blue">(self 'bSize)
or ('bSize super) , ('class #a) , (self 'halt) and (y 'first).
</mark></b></p>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
    <h3 class="panel-title">Primary Value Sharing</h3>
    </div>
    <div class="panel-body">

<p>This unique interplay between the two paradigms, <b>also produces
intresting syntactic and semantic forms</b>, when we
consider <b>the use of primary values that the two languages share</b>:</p>

<p><img src="{{site.baseurl}}/images/lisePolyCalls.png" alt="PolyInvoc"></p>

<p>Notice for example in the above <b>mixed-paradigm unit-test</b> (#assert: and #= messages
are Pharo expressions, while the rest is a Lise expression with scoped Pharo embeddings), 
<b>the use of Pharo block literals and dynamic arrays (seen on line 2):</b></p>

<p><b><mark style="color:blue">([:x | x first - 5] {'string' size})
</mark></b></p>

<p>There is <b>no scoped embedding in line 2</b> (ie (:...) smiley scoping), <b>yet
we can mix pharo code (inside the pharo-block and the dynamic array) with a functional
Lise invocation (the block is the head and will thus be evaluated with the dynamic array
as its argument).</b></p> 

<p>This <b>non-scoped embedding emerges automatically in an environment where
OO expressions and functional expressions share the same primary value syntax 
and semantics.</b></p> 

<small><i>On a side-note: the curious reader might have noticed that <b>the #+ Lise function
is variadic in the above example.</b> This is still another Lise extention, since Pharo does
not natively support variadic lambdas.</i></small>
    </div>
</div>


Primary value sharing is beautifully expressed in **the Lise Recognizer and
Compiler (assembled of course through the use of GrammarTraits)**:

![liseRecognizer]({{site.baseurl}}/images/liseRecognizer.png)

The LiseExpressions Recognizer by itself, operates only over lambdas, lists,
numbers and strings. **The above GrammarTrait composition says that the resulting
sum of the extented PharoGrammar with the LiseExpresssions grammar 
should use the full set of Pharo primary values instead** ! Not shown
above are the **overriden** rules for the composition which in this case
includes an **extented version of what constitutes an \<expression\> for the resulting
language**.

![liseCompiler]({{site.baseurl}}/images/liseCompiler.png)

For **the extented LiseCompiler the GrammarTrait composition is a straight-
forward sum**. Since any **conflicts or overrides where succefully handled at
the recognizer level above**.

As a first step, towards porting larger functional systems
into Lise, for teaching and research. We also **ported P. Norvig's test-suite 
for lispy implementations, to test Lise**:

![liseNorvig]({{site.baseurl}}/images/liseNorvig.png)

Notice the **mixing of Lise expressions with 
the |=? custom pharo operator in the above example** (used as a short-hand for 
assert:equals:) simplifying unit-testing of Lise expressions. 

The punchline in the Lise Postcards
-----------------------------------

As we noted earlier, **smiley scoping `(:..)` can be also used to accomodated
other syntactical extentions** as well. Here **is a Lise postcard mixing 
the imperative extentions we discussed in the** [GrammarTraits]({{site.baseurl}}/GrammarTraits) overview. 
With the exception of the pragma there is **not a single native 
Pharo expression in this method (only functional and procedural extentions)**:

![liseAltPostcard]({{site.baseurl}}/images/liseAltPostcard.png)

And finally the postcard with **all three paradigms combined, Lise (functional) 
Pharo (OO), and our imperative/procedural extentions**:

![liseAltMixedPostcard]({{site.baseurl}}/images/liseAltMixedPostcard.png)

While we are not expecting developers to mix paradigms so aggresively, this
last experiment, **explores the boundaries of what is possible with MOGs, Gray
and GrammarTraits.** It **also shows that our results are generalizable**. They can
also be **applicable in languages with a more imperative (Java, C# etc) or functional base
(Racket, Clojure ...)**, instead of only a pure OO one (like Pharo). 

Indeed in Lise there is now **no real distinction between 
the OO host-language and the functional or procedural extentions, since they are all implemented 
the exact same way (through MOGs, ASL and GrammarTraits)**.
